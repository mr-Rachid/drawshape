<?php

namespace src\tests;
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use src\Rectangle;


/**
 * @coversDefaultClass src\Rectangle
 */

final class RectangleTest extends TestCase{
 
   // accessors for width & height attributes
   /**
    * @covers ::getWidth
    * */
      function testgetWidth():void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $width = $arectangle->getWidth() ;

         $this->assertEquals( 15 ,$width);
      }
   /**
    * @covers ::getHeight
    * */
      function testgetHeight():void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $height = $arectangle->getHeight() ;

         $this->assertEquals( 5 , $height);
      }

      
      /**
       *  Point bottomLeft, Point topRight
       * */     
      /**
       * @covers setBottomleft::
       * */
      function setBottomLeft():void
      {  
         $arectangle = new Rectangle(10, 10, 25, 15);
         $arectangle->setBottomLeft( 15,17) ;

         $this->assertEquals(15 , $arectangle->getX());
         $this->assertEquals(17 , $arectangle->getY());
      }
      /**
       * @covers ::setTopRight::
       * */
      function testsetTopRight():void
      {  
         $arectangle = new Rectangle(10, 10, 25, 15);
         $arectangle->setTopRight(23,32) ;
         
         $this->assertEquals(23 , $arectangle->getX1());
         $this->assertEquals(32 , $arectangle->getY1());
      }

      
      /**
       * @covers getDrawdata
       * */
      function testgetDrawdata()
      {   
         $arectangle = new Rectangle(10, 10, 25, 15);
         $data  = $arectangle->getDrawdata();

         $this->assertIsArray($data ,'test is ok !') ;
      }
   /**
    * @covers ::draw
    * */
      // draw the Rectangle
      function testdraw():void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $surface = $arectangle->getDrawData() ;
         $arectangle->draw($surface);
         
         $this->expectOutputString('<img src="recatangle.png"><br>Drawing a Rectangle at:(10,10), width 15, height 5<br>');

      }
   /**
    * @covers ::area
    * */
       // area function clculater 
      function testarea(): void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $arectangle->area();

         //$this->assertEquals(314.15926535898, $arectangle->area());
         $this->expectOutputString("the area of this recatangle is :" . 75);
      }
   /**
    * @covers ::Drawme
    * */
   function testDrawme():void
   {
      $arectangle = new Rectangle(10, 10, 25, 15);
      $arectangle->Drawme();

      $this->expectOutputString('Drawing a Rectangle at:(10,10), width 15, height 5<br>');
   }

   }


?>