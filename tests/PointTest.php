<?php 
namespace src\tests;
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use src\Rectangle;
use src\Circle;
use src\Point;

/**
 * @coversDefaultClass src\Point
 */

final class PointTest extends TestCase{

      // accessors for x & y coordinates
         
      /**
    * @covers ::getX
    * */
      function testgetX():void 
      {  
         $circle = new Circle(10,15,10);
         $X = $circle->getX() ;

         $this->assertEquals(10 , $X);
      }

      /**
    * @covers ::getY
    * */
      function getY():void 
      {
         $circle = new Circle(10,15,10);
         $Y = $circle->getY() ;

         $this->assertEquals(15 , $Y);

      }
     
      /**
    * @covers ::draw
    * */
      // draw shape Rectangle or Circle
      function testdraw():void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $surface_rec = $arectangle->getDrawData() ;
         $circle = new Circle(10,15,10);
         $surface_circle = $circle->getDrawData() ;

         $arectangle->draw($surface_rec);
         $circle->draw($surface_circle);

         $this->expectOutputString('<img src="recatangle.png"><br>Drawing a Rectangle at:(10,10), width 15, height 5<br><img src="circle.png"><br>Drawing a Circle at:(10,15), radius 10)<br>');
         
      }
   /**
    * @covers ::area
    * */
       // function clculater area 
      function testarea(): void 
      {
         $arectangle = new Rectangle(10, 10, 25, 15);
         $circle = new Circle(10,15,10);
         
         $arectangle->area();
         $circle->area();

         $this->expectOutputString("the area of this recatangle is :". 75 ."the area of this circle is:". 314.15926535898 );
      }
  

   }



?>