<?php 

namespace src\tests;

require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use src\Circle;

/**
 * @coversDefaultClass src\Circle
 */


final class CircleTest extends TestCase {
      // accessors for radius attribute
   /**
    * @covers ::getRadius
    * */
      function testgetRadius():void 
      {
         $circle = new Circle(10,15,10);
         $raduis = $circle->getRadius();
         
         $this->assertEquals(10 , $raduis) ;
      }
      
      /**
       * @covers getDrawdata
       * */
      function testgetDrawdata()
      {   
         $circle = new Circle(10,15,10);
         $data  = $circle->getDrawdata();

         $this->assertIsArray($data ,'test is ok !') ;
      }
   /**
    * @covers ::draw
    * */
      // draw the circle
      function testdraw():void 
      {
         $circle = new Circle(10,15,10);
         $surface = $circle->getDrawData() ;
         $circle->draw($surface);
         
         $this->expectOutputString('<img src="circle.png"><br>Drawing a Circle at:(10,15), radius 10)<br>');

      }
   /**
    * @covers ::area
    * */
       // area function clculater 
      function testarea(): void 
      {
         $circle = new Circle(10,15,10);
         $circle->area();
  
         //$this->assertEquals(314.15926535898, $circle->area());
         $this->expectOutputString("the area of this circle is:". 314.15926535898);
      }


   /**
    * @covers ::Drawme
    * */
   function testDrawme():void
   {
      $circle = new Circle(10, 15,10);
      $circle->Drawme();

      $this->expectOutputString('Drawing a Circle at:(10,15), radius 10)<br>');
   }


}

?>
