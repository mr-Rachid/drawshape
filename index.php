<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
<?php 
require_once __DIR__.'/vendor/autoload.php';

use src\Rectangle;
use src\Circle;

?>
   <title>PHP Test Shapes</title>

</head>
<body>
   <h3> PHP Test draw shape challange </h3>
     <h4> PHP darw in image </h4>
<?PHP

   // access a rectangle specific function
   $arectangle = new Rectangle(25,30,100,120);
   $surface = $arectangle->getDrawData() ;
   //$arectangle->setDrawData(25,30,100,120);
   $arectangle->draw($surface);// or use $arectangle->Drawme();
   $arectangle->area(); // or use echo $arectangle->getArea();

   echo '<br>===================================<br>' ;
   $circle = new Circle(70,80,100);
   $surface = $circle->getDrawData();
   $circle->draw($surface); //$circle->Drawme();
   $circle->area(); // or echo $circle->getArea();
   
   echo '<br>===================================<br>' ;

   $point = new src\Point(200,70) ; 
   $surface = $point->getDrawData();
   $point->draw($surface); //$point->Drawme() ;
 
// array with some shapes instances
/*   $shapse = array(new Rectangle(10, 20, 5, 6), new Circle(15, 25, 8));

   // iterate through the shapes
   for ($i = 0; $i < 2; $i++) {
      $shapse[$i]->draw();
      $shapse[$i]->rMoveTo(100, 100);
      $shapse[$i]->draw();
   }
*/
/*point data */

$point_data =$point->getDrawData();
/*set arectangle data*/
$arectangle->setBottomLeft(30,30);
$arectangle->setTopRight(100,100);
/* get arectangle data */
$rec_data = $arectangle->getDrawdata() ;

/* circle shape */
$circle->setRadius(120);
$circle->setX(120);
$circle->setY(200);
$circle_data = $circle->getDrawdata() ;

$triangle = new src\Triangle(30, 120, 150, 120, 100, 150);  
$trg_data =$triangle->getDrawdata();


?>
 <hr><h4> JS Draw use P5 js pkg </h4><br>
<p>you  can  take a look at the project from here <a href=" https://p5js.org " target="_blank">P5 js</a> </p>

<!-- JS Code -->
<!--  https://p5js.org/get-started/  -->
<script type="">
   function setup() {
  createCanvas(400, 400);
}

var rec_data = <?= json_encode($rec_data) ?>;
var circle_data = <?= json_encode($circle_data) ?>;
var trg_data = <?= json_encode($trg_data) ?>;
var point_data = <?= json_encode($point_data) ?>;

console.log(point_data);
console.log(rec_data);
console.log(circle_data);
console.log(trg_data);
/*check the console */

function draw() {
  background(220);
  stroke('purple'); // Change the color
  strokeWeight(5); // Make the points 10 pixels in 
  
   circle(circle_data.X,circle_data.Y,circle_data.radius);
   rect(rec_data.X, rec_data.Y, rec_data.X1, rec_data.Y1);
   triangle(trg_data.X, trg_data.Y, trg_data.X1, trg_data.Y1,trg_data.X2,trg_data.Y2); 
   point(point_data.X, point_data.Y);
}





</script>
<!-- 
/**
 * TODO 
 * if you  want to move to 3D 
 * you need to  add var Z in the calss Point  
 * point  will be  x,y,z
 * 
 * 
 * TODO 
 *  i create a triangle class  but there is some func messing,  the class need more work and test  
 * like add func get area and draw triangle shape  !
 * need 3 ponts 
 * x,Y
 * x1,y1
 * x2,y2 
 * 
 * getDrawdata function its alredy exsist  
 * 
 * area(),draw() you  need to work at it   
 *  i think there s way to calcumate the area of triangle from x,y cordinate 
 * 
 * Draw Function 
 * i use js function triangle(x,y,x1,y1,x2,y2); from p5 js project
 * 
  **/

  -->
</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/p5@1.4.0/lib/p5.js"></script>