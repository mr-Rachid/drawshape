<?php 

namespace src;

use src\Point;

/**
 * @var circle class
 * 
 * */

   class Circle extends Point{
      private $radius;

      // __constructor
      public function __construct($initx, $inity,$initradius) {
         $this->moveTo($initx, $inity);
         $this->setRadius($initradius);
      }
      /**
       * accessors for radius attribute
       * */
      public function getRadius() {
         return $this->radius;
      }
      
      public function setRadius($newradius) {
         $this->radius = $newradius;
      }
      
      /**
       * get draw data with this Function 
       * */
      public function getDrawdata()
      {
         $data = [ 
            'type'=>'circle',  
            'X'=>$this->getX(),
            'Y'=>$this->getY(), 
            'radius'=> $this->getRadius()];
         return $data;
      }

    /**
     * draw the circle
     * */
      public function Drawme() {
         // Generate image.
         $img = imagecreatetruecolor(255, 255);
         // Create a colour.
         $pink = imagecolorallocate($img, 255, 105, 180);
         // draw a circle
         imageellipse($img, $this->getX() ,$this->getY(),$this->radius,$this->radius, $pink);
            // Save the image to a file.
            imagepng($img, 'circle.png');
            imagedestroy($img);


         echo "Drawing a Circle at:(" . $this->getX() . "," . $this->getY() .
            "), radius " . $this->radius .")<br>";
      }
      /**
      * area function clculater
      **/
      function getArea() 
      {
         return  M_PI * ($this->radius * $this->radius) ;
      }
   

}

?>
