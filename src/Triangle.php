<?php

namespace src;

use src\Point;


/**
 * @var triangle class
 * 
 * */
   class Triangle extends Point{
   
      private $x; private $x1; private $x2; 
      private $y; private $y1; private $y2;
      
      // __constructor 
      public function __construct($initx, $inity ,$initx1,$inity1,$initx2,$inity2) {
         /**
          *  Point bottomLeft, Point topRight
         */
         $this->y = $inity;
         $this->x = $initx;
         $this->x1= $initx1;
         $this->y1 =$inity1;
         $this->x2 =$initx2;
         $this->y2 =$inity2;
      }

      /**
       *  accessors for width & height attributes
       * */
      
      /**
       *  Point 1 
       * */     
      public function setPoint1($initx,$inity)
      {
         $this->x =$initx ;
         $this->y = $inity;
      }
      /**
       * Point 2
       * */
      public function setPoint2($initx1,$inity1)
      {
         $this->x1 = $initx1 ;
         $this->y1 = $inity1;
      }

      /**
       * Point2 
       * */
      public function setPoint3($initx1,$inity1)
      {
         $this->x2 = $initx2 ;
         $this->y2 = $inity2;
      }
      /**
       * get Draw data with this function
       * */
      public function getDrawdata()
      {
         $data = [ 
            'type' =>'triangle',
            'X'=>$this->y,
            'Y'=>$this->x,
            'X1'=>$this->x1,
            'Y1'=>$this->y1,
            'X2'=>$this->x2,
            'Y2'=>$this->y2,
         ];
         return $data;
      }
}