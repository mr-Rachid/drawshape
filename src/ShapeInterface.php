<?php 

namespace src;

interface ShapeInterface{
   function area();
   function draw(array $surface); 
}

?>