<?php

namespace src;

use src\Point;


/**
 * @var Rectangle class
 * 
 * */
   class Rectangle extends Point{
      private $width;
      private $height;
      private $x; private $x1; 
      private $y; private $y1;
      
      // __constructor 
      public function __construct($initx, $inity ,$initx1,$inity1) {
         /**
          *  Point bottomLeft, Point topRight
         */
         $this->setBottomLeft($initx, $inity);
         $this->setTopRight($initx1,$inity1);

         $this->setWidth( $initx1 - $initx);
         $this->setHeight( $inity1 - $inity);
      }

      /**
       *  accessors for width & height attributes
       * */
      public function getWidth() {
         return $this->width;
      }
      public function getHeight() {
         return $this->height;
      }
      public function setWidth($newwidth) {
         $this->width = $newwidth;
      }
      public function setHeight($newheight) {
         $this->height = $newheight;
      }
      
      public function getX1()
      {
         return $this->x1 ;
      }
       
      public function getY1()
      {
         return $this->y1 ;
      }

      /**
       *  Point bottomLeft 
       * */     
      public function setBottomLeft($initx,$inity)
      {
         $this->x =$initx ;
         $this->y = $inity;
      }
      /**
       * Point topRight
       * */
      public function setTopRight($initx1,$inity1)
      {
         $this->x1 = $initx1 ;
         $this->y1 = $inity1;
      }
      /**
       * get Draw data with this function
       * */
      public function getDrawdata()
      {
         $data = [ 
            'type' =>'rectangle',
            'X'=>$this->y,
            'Y'=>$this->x,
            'X1'=>$this->x1,
            'Y1'=>$this->y1,
            'height'=> $this->getHeight(),
            'width'=>$this->getWidth(),
         ];
         return $data;
      }
      
      /**
       * setDrawdata Function 
       * */
      public function setDrawdata($data)
      {
         $this->setBottomLeft($data['X'], $data['Y']);
         $this->setTopRight($data['X1'], $data['Y1']);

         $this->setWidth( $data['X1']- $data['X']);
         $this->setHeight( $data['Y1']- $data['Y']);
      }


      /**
       * draw the rectangle
       * */
      public function Drawme() {
         // Generate image.
         $img = imagecreatetruecolor(255, 255);
         // Create a colour.
         $pink = imagecolorallocate($img, 255, 105, 180);
         imageellipse($img, $this->x ,$this->y,$this->x1 ,$this->y1, $pink);
         // Save the image to a file.
         imagepng($img, 'circle.png');
         imagedestroy($img);

         echo "Drawing a Rectangle at:(" .$this->x . "," . $this->y ."), width " .
                   $this->getWidth() . ", height " . $this->getHeight() . "<br>";
      }
      /**
       * area function clculater
      */
      public function getArea() 
      {
         return $this->getWidth() * $this->getHeight()  ;
      }


}


?>