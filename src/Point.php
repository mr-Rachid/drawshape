<?php 
namespace src;


use src\Shape;
/**
 * 
Example pseudo-code:
 forall shapes as shape, do shape->draw(surface)
 forall shapes as shape, do surface.draw(shape) (where shape.getDrawData(), or shape.getPath())
 * */
class Point extends Shape{
      private $x;
      private $y;

      // constructor
      public function __construct ($initx, $inity)
      {
         $this->moveTo($initx, $inity);
      }

      // accessors for x & y coordinates
      public function getX() {
         return $this->x;
      }
      public function getY() {
         return $this->y;
      }
      public function setX($newx) {
         $this->x = $newx;
      }
      public function setY($newy) {
         $this->y = $newy;
      }

      // modify the shape coordinates
      public function moveTo($newx, $newy) {
         $this->setX($newx);
         $this->setY($newy);
      }
      public function rMoveTo($deltax, $deltay) {
         $this->moveTo(($this->getX() + $deltax), ($this->getY() + $deltay));
      }
        /**
       * get draw data with this Function 
       * */
      public function getDrawdata()
      {
         $data = [ 
            'type'=>'point',  
            'X'=>$this->getX(),
            'Y'=>$this->getY(), 
         ];

         return $data;
      }

    /**
     * draw the point
     * */
      public function Drawme() {
         // Generate image.
         $img = imagecreatetruecolor(255, 255);
         // Create a colour.
         $pink = imagecolorallocate($img, 255, 105, 180);
         // draw a circle
         imagesetpixel($img, $this->getX() ,$this->getY(), $pink);
            // Save the image to a file.
            imagepng($img, 'point.png');
            imagedestroy($img);
            echo '<img src="point.png"><br>';
         echo "Drawing a Point at:(" . $this->getX() . "," . $this->getY().")<br>";
      }

}  


?>