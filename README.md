# DrawShape Challange 

```
 _                          _____                                 
| |    (_)                 /  ___|                                
| |     _  _ __ ___    ___ \ `--.  _   _  _ __ __   __ ___  _   _ 
| |    | || '_ ` _ \  / _ \ `--. \| | | || '__|\ \ / // _ \| | | |
| |____| || | | | | ||  __//\__/ /| |_| || |    \ V /|  __/| |_| |
\_____/|_||_| |_| |_| \___|\____/  \__,_||_|     \_/  \___| \__, |
                                                             __/ |
                                                            |___/ 
```   



# usage 
Rectangle: 
```
   // access a rectangle specific function
   $arectangle = new Rectangle(25,30,100,120);
   $surface = $arectangle->getDrawData() ;
   //$arectangle->setDrawData(25,30,100,120);
   $arectangle->draw($surface);// or use $arectangle->Drawme();
   $arectangle->area(); // or use echo $arectangle->getArea();
```
  Circle:
```   
   $circle = new Circle(70,80,100);
   $surface = $circle->getDrawData();
   $circle->draw($surface); //$circle->Drawme();
   $circle->area(); // or echo $circle->getArea();
```   
  Point:
```
   $point = new src\Point(200,70) ; 
   $surface = $point->getDrawData();
   $point->draw($surface); //$point->Drawme() ;
```
Triangle
```
TODO 
you need to  complite the work at the func draw in the class shape  

you  can get data with getDrawdata() fun and drwa the  shape with the P5 js 
```
// array with some shapes instances
```
$shapse = array(new Rectangle(10, 20, 5, 6), new Circle(15, 25, 8));

   // iterate through the shapes
   for ($i = 0; $i < 2; $i++) {
      $shapse[$i]->draw();
      $shapse[$i]->rMoveTo(100, 100);
      $shapse[$i]->draw();
   }
```

Result expectd !

![text](/drawphp.PNG)

how to  go and use JS code
Data 
```
/*point data */

$point_data =$point->getDrawData();
/*set arectangle data*/
```

Rectangle data
```
$arectangle->setBottomLeft(30,30);
$arectangle->setTopRight(100,100);
/* get arectangle data */
$rec_data = $arectangle->getDrawdata() ;

```
circle shape 
```
/* circle shape */
$circle->setRadius(120);
$circle->setX(120);
$circle->setY(200);
$circle_data = $circle->getDrawdata() ;
```
Triangle
```
$triangle = new src\Triangle(30, 120, 150, 120, 100, 150);  
$trg_data =$triangle->getDrawdata();
```

JS Code P5 js pkg
```
<!--  https://p5js.org/get-started/  -->
<script type="">
   function setup() {
  createCanvas(400, 400);
}

var rec_data = <?= json_encode($rec_data) ?>;
var circle_data = <?= json_encode($circle_data) ?>;
var trg_data = <?= json_encode($trg_data) ?>;
var point_data = <?= json_encode($point_data) ?>;
```

```
console.log(point_data);
console.log(rec_data);
console.log(circle_data);
console.log(trg_data);
/*check the console */

function draw() {
  background(220);
  stroke('purple'); // Change the color
  strokeWeight(5); // Make the points 10 pixels in 
  
   circle(circle_data.X,circle_data.Y,circle_data.radius);
   rect(rec_data.X, rec_data.Y, rec_data.X1, rec_data.Y1);
   triangle(trg_data.X, trg_data.Y, trg_data.X1, trg_data.Y1,trg_data.X2,trg_data.Y2); 
   point(point_data.X, point_data.Y);
}
```
Result expectd !
![text area](drawjs.PNG)
```
/**
 * TODO 
 * if you  want to move to 3D 
 * you need to  add var Z in the calss Point  
 * point  will be  x,y,z
 * 
 * 
 * TODO 
 *  i create a triangle class  but there is some func messing,  the class need more work and test  
 * like add func get area and draw triangle shape  !
 * need 3 ponts 
 * x,Y
 * x1,y1
 * x2,y2 
 * 
 * getDrawdata function its alredy exsist  
 * 
 * area(),draw() you  need to work at it   
 *  i think there s way to calculate the area of triangle from x,y cordinate 
 * 
 * Draw Function 
 * i use js function triangle(x,y,x1,y1,x2,y2); from p5 js project
 * 
 ```

# Test Code 
folder files tests
run code 
```
./vendor/bin/phpunit 
```

# Code coverage 
folder coverage

run code  
```
./vendor/bin/phpunit --coverage-html  coverage
```
